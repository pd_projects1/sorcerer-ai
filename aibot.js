import Replicate from 'replicate';
import readline from 'readline';
import say from 'say';

const replicate = new Replicate({
  auth: process.env.REPLICATE_API_TOKEN,
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let conversationBuffer = [];

const runReplicate = async (userInput) => {
  // Append the latest user input to the buffer
  conversationBuffer.push(`USER: ${userInput}`);
  if (conversationBuffer.length > 20) {
    conversationBuffer = conversationBuffer.slice(-20); // Keep only the last 20 exchanges
  }

  // Generate the prompt with "SORCERER:" at the end
  const prompt = conversationBuffer.join("\n") + "\nSORCERER:";

  // Run the model and get the output
  const response = await replicate.run(
    "asteenkamer/zolo:be4b51d1f906d6aa54a7d578f117dd0ed895ec34dff1ee3315a6893143078e29",
    {
      input: {
        n: 1,
        top_p: 1,
        prompt: prompt,
        max_length: 500,
        temperature: 0.75,
        repetition_penalty: 1
      }
    }
  );

  // Extract the first element from the response array
  const output = response[0];

  // Find the SORCERER response that immediately follows the latest USER input
  const outputLines = output.split("\n");
  const userIndex = outputLines.lastIndexOf(`USER: ${userInput}`);
  const sorcererResponse = outputLines.slice(userIndex + 1).find(line => line.startsWith("SORCERER:"));

  // Add the SORCERER response to the buffer, print it, and speak it
  if (sorcererResponse) {
    conversationBuffer.push(sorcererResponse);
    console.log(sorcererResponse);
    // Extract the response text without "SORCERER:"
    let responseText = sorcererResponse.replace("SORCERER: ", "");

    // Add a short pause or word at the end to prevent audio clipping
    responseText += " ...hmm";

    // Use 'say' to speak out the modified response
    say.speak(responseText);
  } else {
    console.log("No new SORCERER response found.");
  }
};


const askQuestion = () => {
  rl.question('USER: ', async (userInput) => {
    if (userInput.toLowerCase() === 'exit') {
      rl.close();
    } else {
      await runReplicate(userInput);
      askQuestion(); // Ask the next question
    }
  });
};

askQuestion(); // Start the conversation

